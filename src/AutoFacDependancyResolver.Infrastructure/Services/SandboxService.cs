﻿using AutofacDependancyResolver.Core.Domain.Services;

namespace AutofacDependancyResolver.Infrastructure.Services
{
    public class SandboxService : ISandboxService
    {
        public void DoSomething()
        {
        }
    }
}