﻿namespace AutofacDependancyResolver.Core.Domain.Services
{
    public interface ISandboxService
    {
        void DoSomething();
    }
}