﻿using System.Web.Mvc;
using AutofacDependancyResolver.Core.Domain.Services;
using Sitecore.Mvc.Controllers;

namespace AutofacDependancyResolver.Web.Controllers
{
    public class SandboxController : SitecoreController
    {
        private readonly ISandboxService _sandboxService;

        public SandboxController(ISandboxService sandboxService)
        {
            _sandboxService = sandboxService;
        }

        public ActionResult SandboxAction()
        {
            _sandboxService.DoSomething();

            return new EmptyResult();
        }
    }
}