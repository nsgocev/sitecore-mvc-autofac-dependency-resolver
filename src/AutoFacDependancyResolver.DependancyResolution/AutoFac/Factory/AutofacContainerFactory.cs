﻿using System;
using Autofac;
using Autofac.Integration.Mvc;

namespace AutofacDependancyResolver.DependancyResolution.AutoFac.Factory
{
    public class AutofacContainerFactory
    {
        public IContainer Create()
        {
            var builder = new ContainerBuilder();

            // Register All Controllers In The Current Scope
            builder.RegisterControllers(AppDomain.CurrentDomain.GetAssemblies());

            // Register Modules
            builder.RegisterModule<ServicesModule>();

            // Register Additional Things
            // ...

            // Build The Container and return it as a result
            return builder.Build();
        }
    }
}