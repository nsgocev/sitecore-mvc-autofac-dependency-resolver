﻿using Autofac;
using AutofacDependancyResolver.Core.Domain.Services;
using AutofacDependancyResolver.Infrastructure.Services;

namespace AutofacDependancyResolver.DependancyResolution.AutoFac
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SandboxService>().As<ISandboxService>();
        }
    }
}