﻿using System.Web.Mvc;
using Autofac.Integration.Mvc;
using AutofacDependancyResolver.DependancyResolution.AutoFac.Factory;
using Sitecore.Pipelines;

namespace AutofacDependancyResolver.DependancyResolution.AutoFac.Pipelines
{
    public class InitializeAutofacControllerFactory
    {
        public virtual void Process(PipelineArgs args)
        {
            SetControllerFactory(args);
        }

        private void SetControllerFactory(PipelineArgs args)
        {
            var containerFactory = new AutofacContainerFactory();
            var container = containerFactory.Create();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            var controllerFactory = new AutofacControllerFactory(container);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }
    }
}